module.exports = {
  extends: 'airbnb',
  rules: {
    'import/named': 0,
    'import/extensions': 0,
    'import/no-unresolved': 0,
    'no-restricted-syntax': 0,
    'no-underscore-dangle': 0,
  },
};
