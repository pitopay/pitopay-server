export default {
  port: process.env.PORT || '8080',
  txlist: 'https://api.pchain.org/getScanTxList',
  ratesApi: 'https://api.ratesapi.io/api/latest?base=USD',
  dbAddress: process.env.DB || 'mongodb://127.0.0.1/pitopay',
};
