import mongoose, { Schema } from 'mongoose';

export default mongoose.model('Payment', Schema({
  currency: {
    trim: true,
    type: String,
    maxlength: 50,
    required: true,
  },
  amount: {
    tirm: true,
    type: String,
    required: true,
    maxlength: 100,
  },
  address: {
    trim: true,
    type: String,
    required: true,
    maxlength: 300,
  },
  createdAt: {
    trim: true,
    type: Date,
    required: true,
    default: Date.now,
  },
}));
