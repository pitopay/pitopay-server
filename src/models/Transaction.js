import mongoose, { Schema } from 'mongoose';

export default mongoose.model('Transaction', Schema({
  payment: {
    trim: true,
    required: true,
    ref: 'Payment',
    maxlength: 200,
    type: Schema.Types.ObjectId,
  },
  pi: {
    trim: true,
    type: Number,
    required: true,
  },
  hash: {
    trim: true,
    type: String,
    maxlength: 500,
    required: false,
  },
  comment: {
    trim: true,
    type: String,
    maxlength: 20,
    required: true,
  },
  status: {
    type: String,
    default: 'pending',
    enum: ['failure', 'success', 'pending'],
  },
  createdAt: {
    trim: true,
    type: Date,
    required: true,
    default: Date.now,
  },
}));
