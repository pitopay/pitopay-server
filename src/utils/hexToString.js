export default (str1) => {
  let str = '';

  const hex = str1.toString();

  for (let n = 0; n < hex.length; n += 2) {
    str += String.fromCharCode(parseInt(hex.substr(n, 2), 16));
  }

  return str;
};
