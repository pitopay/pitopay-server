import fx from 'money';
import fetch from 'node-fetch';

import { ratesApi } from 'Root/config';

export default (amount, from, to) => new Promise((resolve, reject) => {
  fetch(ratesApi)
    .then((res) => res.json())
    .then((data) => {
      fx.base = data.base;
      fx.rates = data.rates;

      const result = fx(Number.parseFloat(amount, 10))
        .from(from)
        .to(to);

      resolve(result);
    })
    .catch((e) => {
      reject(e);
    });
});
