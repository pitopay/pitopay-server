import hexToString from './hexToString';

export default (data, amount, createdAt, comment) => {
  const timestamp = Number.parseInt(new Date(createdAt).getTime(), 10) / 1000;

  const filteredData = data.filter((x) => Number.parseInt(x.timestamp, 10) > timestamp);

  const isPaid = filteredData.filter((x) => Number.parseInt(x.value, 10)
    === amount * 1000000000000000000
    && hexToString(x.input).includes(comment));

  return isPaid;
};
