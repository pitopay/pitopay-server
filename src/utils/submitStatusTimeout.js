import Transaction from 'Root/models/Transaction';

import submitStatus from './submitStatus';

export default async () => {
  const transactions = await Transaction.find({ status: 'pending' });

  for (const transaction of transactions) {
    const time = new Date(transaction.createdAt).getTime() / 1000;
    const now = new Date().getTime() / 1000;
    const fifteenMinutes = 15 * 60;

    const remainingTime = Number.parseInt(fifteenMinutes - (now - time), 10);

    setTimeout(() => {
      submitStatus(transaction._id);
    }, remainingTime * 1000);
  }
};
