import Web3 from 'pweb3';

export default (address) => {
  const web3 = new Web3('http://3.testnet.plian.io:6969/child_0');

  return web3.utils.isAddress(address);
};
