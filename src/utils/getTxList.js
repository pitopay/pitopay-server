import fetch from 'node-fetch';

import { txlist } from 'Root/config';

export default (address) => new Promise((resolve, reject) => {
  const body = {
    address,
    pageNo: 1,
    chainId: 0,
    pageSize: 50,
  };

  fetch(txlist, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  })
    .then((res) => res.json())
    .then((data) => {
      if (data.result !== 'success') {
        reject({});
      } else {
        resolve(data.data);
      }
    })
    .catch((e) => {
      reject(e);
    });
});
