import Transaction from 'Root/models/Transaction';

export default async (id) => {
  try {
    const transaction = await Transaction.findById(id);

    if (transaction.status !== 'success') {
      transaction.status = 'failure';

      await transaction.save();
    }
  } catch (error) {
    console.error(error.message);
  }
};
