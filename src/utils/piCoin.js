import fetch from 'node-fetch';

export default (usd) => new Promise((resolve, reject) => {
  fetch(`https://web-api.coinmarketcap.com/v1/tools/price-conversion?amount=${usd}&convert_id=2838&id=2781`)
    .then((res) => res.json())
    .then((d) => {
      resolve(d.data.quote['2838'].price);
    })
    .catch((e) => {
      reject(e);
    });
});
