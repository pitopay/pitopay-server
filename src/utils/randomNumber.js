export default (length) => {
  let str = '';

  for (let i = 0; i < length; i += 1) {
    str += Math.round(Math.random() * 10);
  }

  return str;
};
