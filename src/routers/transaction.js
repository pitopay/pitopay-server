import { Router } from 'express';

import piCoin from 'Root/utils/piCoin';
import Payment from 'Root/models/Payment';
import exchange from 'Root/utils/exchange';
import Transaction from 'Root/models/Transaction';
import submitStatus from 'Root/utils/submitStatus';
import randomNumber from 'Root/utils/randomNumber';
import paramsRequirements from 'Root/middlewares/requirements/params';

const router = new Router();

const params = paramsRequirements({
  value: 'code',
  required: true,
});

router.post('/transaction/:code', params, async (req, res) => {
  try {
    const checkPayment = await Payment.findById(req.params.code);

    if (!checkPayment) {
      const statusCode = 404;

      return res.status(statusCode).json({
        statusCode,
        entity: 'code',
        message: 'code is not found',
      });
    }

    let pi;

    if (checkPayment.currency === 'PI') {
      pi = checkPayment.amount;
    } else {
      const usdPrice = await exchange(checkPayment.amount, checkPayment.currency, 'USD');
      const toPI = await piCoin(usdPrice);
      pi = toPI.toFixed(2);
    }


    const transaction = new Transaction({
      pi,
      comment: randomNumber(10),
      payment: checkPayment._id,
    });

    await transaction.save();

    setTimeout(() => {
      submitStatus(transaction._id);
    }, 1000 * 60 * 15);

    const statusCode = 200;

    return res.status(statusCode).json({
      statusCode,
      data: {
        pi,
        comment: transaction.comment,
        paymentLink: transaction._id,
        address: checkPayment.address,
      },
    });
  } catch (error) {
    const statusCode = 404;

    return res.status(statusCode).json({
      statusCode,
      entity: 'code',
      message: error.message,
    });
  }
});

export default router;
