import { Router } from 'express';

import Payment from 'Root/models/Payment';
import currencies from 'Root/utils/currencies';
import addressValidator from 'Root/utils/addressValidator';
import bodyRequirements from 'Root/middlewares/requirements/body';

const router = new Router();

const reqs = bodyRequirements(
  {
    value: 'currency',
    required: true,
  },
  {
    value: 'amount',
    required: true,
  },
  {
    value: 'address',
    required: true,
  },
);

router.post('/payment', reqs, async (req, res) => {
  try {
    if (!addressValidator(req.body.address)) {
      const statusCode = 400;

      return res.status(statusCode).json({
        statusCode,
        entity: 'address',
        message: 'Address is invalid',
      });
    }

    if (currencies.indexOf(req.body.currency) === -1) {
      const statusCode = 400;

      return res.status(statusCode).json({
        statusCode,
        entity: 'currency',
        message: 'Currency is invalid',
      });
    }

    const payment = new Payment({
      amount: req.body.amount,
      address: req.body.address,
      currency: req.body.currency,
    });

    await payment.save();

    const statusCode = 200;

    return res.status(statusCode).json({
      statusCode,
      data: {
        _id: payment._id,
        amount: payment.amount,
        address: payment.address,
        currency: payment.currency,
      },
      message: 'Payment has been created',
    });
  } catch (error) {
    return res.status(500).json({
      error: error.message,
    });
  }
});

export default router;
