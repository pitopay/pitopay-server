import { Router } from 'express';

import isPaid from 'Root/utils/isPaid';
import getTxList from 'Root/utils/getTxList';
import Transaction from 'Root/models/Transaction';
import paramsRequirements from 'Root/middlewares/requirements/params';

const router = new Router();

const params = paramsRequirements({
  value: 'code',
  required: true,
});

router.post('/transaction/:code/status', params, async (req, res) => {
  try {
    const checkPayment = await Transaction
      .findById(req.params.code)
      .populate('payment')
      .exec();

    if (!checkPayment) {
      const statusCode = 404;

      return res.status(statusCode).json({
        statusCode,
        entity: 'code',
        message: 'code is not found',
      });
    }

    if (checkPayment.status !== 'pending') {
      return res.json({
        statusCode: 200,
        pi: checkPayment.pi,
        status: checkPayment.status,
        comment: checkPayment.comment,
        hash: checkPayment.hash || null,
        createdAt: checkPayment.createdAt,
        address: checkPayment.payment.address,
        isPaid: checkPayment.status === 'success',
      });
    }

    const txList = await getTxList(checkPayment.payment.address);
    const checkIsPaid = await isPaid(
      txList,
      checkPayment.pi,
      checkPayment.createdAt,
      checkPayment.comment,
    );

    if (checkIsPaid.length) {
      checkPayment.status = 'success';
      checkPayment.hash = checkIsPaid[0].hash;

      await checkPayment.save();
    }

    return res.json({
      statusCode: 200,
      pi: checkPayment.pi,
      status: checkPayment.status,
      isPaid: !!checkIsPaid.length,
      comment: checkPayment.comment,
      hash: checkPayment.hash || null,
      createdAt: checkPayment.createdAt,
      address: checkPayment.payment.address,
    });
  } catch (error) {
    const statusCode = 404;

    return res.status(statusCode).json({
      statusCode,
      entity: 'code',
      message: error.message,
    });
  }
});

export default router;
