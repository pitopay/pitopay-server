import { Router } from 'express';

import Payment from 'Root/models/Payment';
import paramsRequirements from 'Root/middlewares/requirements/params';

const router = new Router();

const params = paramsRequirements({
  value: 'code',
  required: true,
});

router.post('/payment/:code/exist', params, async (req, res) => {
  try {
    const checkPayment = await Payment.findById(req.params.code);

    if (!checkPayment) {
      const statusCode = 404;

      return res.status(statusCode).json({
        statusCode,
        entity: 'code',
        message: 'code is not found',
      });
    }

    const statusCode = 200;

    return res.status(statusCode).json({
      statusCode,
    });
  } catch (error) {
    const statusCode = 404;

    return res.status(statusCode).json({
      statusCode,
      entity: 'code',
      message: error.message,
    });
  }
});

export default router;
